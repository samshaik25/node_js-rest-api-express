const express=require('express');

const app=express();

app.use(express.json())


app.get("/", (req, res) => {
    res.send({ message: "Welcome to bezkoder application." });
  });

  require("./app/routes/customer.routes.js")(app);


  const port =process.env.PORT || 3000
app.listen(port,()=>{
    console.log(`listening on  port ${port}..`)
})