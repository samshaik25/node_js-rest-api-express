const pgrs=require("./db")


const Customer = function(customer) {
  this.email = customer.email;
  this.name = customer.name;
  this.active = customer.active;
};

// //////////////////////        POST   ///////////////////////

Customer.create = (newCustomer, result) => {
  console.log(newCustomer);
  pgrs.query("INSERT INTO customers (email,name,active) values($1,$2,$3)",
  [newCustomer.email,newCustomer.name,newCustomer.active], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    console.log("created customer: ", { id: res.insertId, ...newCustomer });
    result(null, { id: res.insertId, ...newCustomer });
  });
};


// ///////////////////////              GET  ALL CUSTOMERS             //////////////////



Customer.getAll = result => {
  pgrs.query("SELECT * FROM customers", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("customers: ", res.rows);
    result(null, res.rows);
  });
};


  
  
// ///////////////////////////        GET  CUSTOMER BY ID               ////////////////////////

Customer.findById = (customerId, result) => {
  pgrs.query(`SELECT * FROM customers WHERE id = ${customerId}`, (err, res) => {
    console.log("errr",err)
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }
    console.log("ress",res)
    if (res.rows.length) {
      console.log("found customer: ", res[0]);
      result(null, res.rows);
      return;
    }
    // result(null,res.rows)
  else{

    result({ kind: "not_found" }, null);
  }
    // not found Customer with the id
  });
};



// //////////////////////////////       DELETE BY ID         /////////////////////////

Customer.remove = (id, result) => {
  pgrs.query(`DELETE FROM customers WHERE id =${id} RETURNING *`,  (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    if (res.affectedRows == 0) {
      // not found Customer with the id
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("deleted customer with id: ", id);
    result(null, res);
  });
};


// ////////////////////////////  UPDATE          ///////////////////////

Customer.updateById = (id, customer, result) => {
  pgrs.query(
    "UPDATE customers SET email =$1 , name = $2, active = $3 WHERE id = $4",
    [customer.email, customer.name, customer.active, id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        // not found Customer with the id
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated customer: ", { id: id, ...customer });
      result(null, { id: id, ...customer });
    }
  );
};




// //////////////////////////         DELETE ALL       /////////////


Customer.removeAll = result => {
  pgrs.query("DELETE FROM customers", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log(`deleted ${res.affectedRows} customers`);
    result(null, res);
  });
};

 module.exports = Customer;
