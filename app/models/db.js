var pg = require('pg');
//or native libpq bindings
//var pg = require('pg').native
const dbConfig = require("../config/db.config.js");
// console.log(dbConfig.URL)

var conString = dbConfig.URL //Can be found in the Details page
var client = new pg.Client(conString);


client.connect(error => {
  if (error) throw error;
  console.log("Successfully connected to the database.");
});

// client.connect(function(err) {
//     if(err) {
//       return console.error('could not connect to postgres', err);
//     }
//     client.query('SELECT NOW() AS "theTime"', function(err, result) {
//       if(err) {
//         return console.error('error running query', err);
//       }
//       console.log(result.rows[0].theTime);
//       // >> output: 2018-08-23T14:02:57.117Z
//       client.end();
//     });
//   });

// connect(client)

module.exports=client